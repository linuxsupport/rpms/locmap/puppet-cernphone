class cernphone::install {

  # appImages are the future (!)
  $appimage_path = 'https://cernphone-sw.web.cern.ch/cernphone-sw/releases/cern-phone-app-1.3.0-x86_64-linux.AppImage.d/cern-phone-app.AppImage'

  $appimage_deps = [ 'fuse', 'fuse-libs' ]
  $cernphone_deps = [ 'alsa-lib', 'libsecret' ]

  ensure_packages($appimage_deps, {ensure => present})
  ensure_packages($cernphone_deps, {ensure => present})

  # use ensure_resource as we may reuse this code in other modules (cernphone)
  ensure_resource('file', '/usr/local/appimages',
  {
    'ensure' => 'directory',
    # 777 ensures that users can update appImages through the app itself
    'mode' => '0777',
  })
  ensure_resource('file', '/usr/local/share/icons',
  {
    'ensure' => 'directory',
  })
  file { '/usr/local/appimages/cernphone.AppImage':
    ensure  => 'file',
    # 777 ensures that users can update appImages through the app itself
    mode    => '0777',
    source  => $appimage_path,
    require => File['/usr/local/appimages'],
  }
  file { '/usr/local/bin/cernphone':
    ensure  => link,
    target  => '/usr/local/appimages/cernphone.AppImage',
    require => File['/usr/local/appimages/cernphone.AppImage'],
  }
  file { '/usr/local/share/icons/cernphone-icon.png':
    ensure  => present,
    source  => "puppet:///modules/cernphone/cernphone-icon.png",
    require => File['/usr/local/share/icons'],
  }
  file { '/usr/share/applications/cernphone.desktop':
    ensure  => present,
    source  => "puppet:///modules/cernphone/cernphone.desktop",
  }
}
