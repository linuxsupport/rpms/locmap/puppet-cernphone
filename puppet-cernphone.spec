Name:           puppet-cernphone
Version:        1.3
Release:        1%{?dist}
Summary:        Masterless puppet module for cernphone client

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch

Requires:       puppet-agent

%description
Masterless puppet module for cernphone

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/cernphone/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/cernphone/
touch %{buildroot}/%{_datadir}/puppet/modules/cernphone/linuxsupport

%files -n puppet-cernphone
%{_datadir}/puppet/modules/cernphone
%doc code/README.md

%post
MODULE=$(echo %{name} | cut -d \- -f2)
if [ -f %{_datadir}/puppet/modules/${MODULE}/linuxsupport ]; then
  grep -qE "autoreconfigure *= *True" /etc/locmap/locmap.conf && AUTORECONFIGURE=1 || :
  if [ $AUTORECONFIGURE ]; then
    locmap --list |grep $MODULE |grep -q enabled && MODULE_ENABLED=1 || :
    if [ $MODULE_ENABLED ]; then
      echo "locmap autoreconfigure enabled, running: locmap --configure $MODULE"
      locmap --configure $MODULE || :
    else
      echo "locmap autoreconfigure enabled, however the $MODULE module is not enabled"
      echo "Skipping (re)configuration of $MODULE"
    fi
  fi
fi

%changelog
* Tue Oct 08 2024 Ben Morrice <ben.morrice@cern.ch> 1.3-1
- Add autoreconfigure %post script

* Wed Oct 11 2023 Ben Morrice <ben.morrice@cern.ch> 1.2-1
- Update to appImage version 1.3.0

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 1.1-2
- Bump release for disttag change

* Thu Jul 21 2022 Ben Morrice <ben.morrice@cern.ch> 1.1-1
- Add icons and desktop shortcut

* Wed Jul 20 2022 Ben Morrice <ben.morrice@cern.ch> 1.0-3
- Add libsecret dependency

* Wed Jul 20 2022 Ben Morrice <ben.morrice@cern.ch> 1.0-2
- Install appImage dependencies

* Wed Jul 20 2022 Ben Morrice <ben.morrice@cern.ch> 1.0-1
- Initial Release
